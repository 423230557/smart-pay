package com.founder.service.impl;

import com.founder.core.dao.RefundOrderRespository;
import com.founder.core.domain.MchNotify;
import com.founder.core.domain.PayOrder;
import com.founder.core.domain.RefundOrder;
import com.founder.core.log.MyLog;
import com.founder.service.IMchNotifyService;
import com.founder.service.IPayOrderService;
import com.founder.service.IRefundOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Transactional
@Service
public class RefundOrderServiceImpl implements IRefundOrderService {

    private final MyLog _log = MyLog.getLog(RefundOrderServiceImpl.class);

    @Autowired
    RefundOrderRespository refundOrderRespository;

    @Autowired
    IMchNotifyService mchNotifyService;

    @Autowired
    IPayOrderService payOrderService;

    @Override
    public RefundOrder selectRefundOrder(String refundOrderId) {
        _log.info("查询退款订单：{}", refundOrderId);
        Optional<RefundOrder> optional = refundOrderRespository.findById(refundOrderId);
        RefundOrder refundOrder = null;
        if (optional.isPresent()){
            refundOrder = optional.get();
        }
        return refundOrder;
    }

    @Override
    public int createRefundOrder(RefundOrder refundOrder) {
        String payOrderId = refundOrder.getPayOrderId();
        PayOrder payOrder = payOrderService.selectPayOrder(payOrderId);
        if (payOrder == null){
            _log.error("退款订单对应的支付订单{}不存在", payOrderId);
            return 0;
        }
        Long refundAmount = refundOrder.getRefundAmount();
        if (refundAmount.longValue() != payOrder.getAmount().longValue()){
            _log.warn("部分退");
        }
        String refundOrderId = refundOrder.getRefundOrderId();
        if (refundOrderRespository.existsById(refundOrderId)){
            _log.error("退款订单{}已经存在", refundOrderId);
            return 0;
        }
        refundOrder.setMchRefundNo(refundOrderId);
        refundOrder.setChannelPayOrderNo(payOrder.getChannelOrderNo());
        refundOrder.setChannelId(payOrder.getChannelId());
        refundOrder.setPayAmount(payOrder.getAmount());
        refundOrder.setChannelMchId(payOrder.getChannelMchId());
        refundOrder.setUpdateTime(new Date());
        refundOrder.setCreateTime(new Date());
        refundOrderRespository.save(refundOrder);

        return 1;
    }

    @Override
    public int updateNotify4Count(String refundOrderId, Integer cnt) {
        RefundOrder refundOrder = this.selectRefundOrder(refundOrderId);
        if (refundOrder == null){
            _log.error("退款订单{}不存在",refundOrderId);
            return 0;
        }

        _log.info("创建或更新商户通知");
        MchNotify mchNotify = mchNotifyService.selectMchNotify(refundOrderId);
        if (mchNotify == null){
            mchNotify = new MchNotify();
            mchNotify.setOrderId(refundOrderId);
            mchNotify.setMchId(refundOrder.getMchId());
            mchNotify.setMchOrderNo(refundOrder.getMchRefundNo());
            mchNotify.setOrderType("3");//'订单类型:1-支付,2-转账,3-退款'
            mchNotify.setNotifyUrl(refundOrder.getNotifyUrl());
            mchNotify.setResult("通知中");//通知响应结果
            mchNotify.setStatus(1);//通知状态,1-通知中,2-通知成功,3-通知失败
            mchNotify.setCreateTime(new Date());
        }
        mchNotify.setStatus(2);
        mchNotify.setNotifyCount(cnt);
        mchNotify.setLastNotifyTime(new Date());
        int result = mchNotifyService.saveMchNotify(mchNotify);

        return result;
    }
}
