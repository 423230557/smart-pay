package com.founder.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.founder.core.domain.MchNotify;
import com.founder.core.log.MyLog;
import com.founder.core.page.PageModel;
import com.founder.core.utils.DateUtil;
import com.founder.service.IMchNotifyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/mch_notify")
public class MchNotifyController {

    private final static MyLog _log = MyLog.getLog(MchNotifyController.class);

    @Autowired
    private IMchNotifyService mchNotifyService;

    @RequestMapping("/list.html")
    public String listInput(ModelMap model) {
        return "mch_notify/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute MchNotify mchNotify, Integer pageIndex, Integer pageSize) {
        _log.info("分页查询通知列表，参数pageIndex={}，pageSize={}。", pageIndex, pageSize);
        PageModel pageModel = new PageModel();
        Page<MchNotify> page = mchNotifyService.selectMchNotifyList((pageIndex-1)*1, pageSize, mchNotify);
        int count = Long.valueOf(page.getTotalElements()).intValue();
        if(count <= 0) return JSON.toJSONString(pageModel);
        List<MchNotify> mchNotifyList = page.getContent();
        if(!CollectionUtils.isEmpty(mchNotifyList)) {
            JSONArray array = new JSONArray();
            for(MchNotify po : mchNotifyList) {
                JSONObject object = (JSONObject) JSONObject.toJSON(po);
                if(po.getCreateTime() != null) object.put("createTime", DateUtil.date2Str(po.getCreateTime()));
                if(po.getLastNotifyTime() != null) object.put("lastNotifyTime", DateUtil.date2Str(po.getLastNotifyTime()));
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/view.html")
    public String viewInput(String orderId, ModelMap model) {
        MchNotify item = null;
        if(StringUtils.isNotBlank(orderId)) {
            item = mchNotifyService.selectMchNotify(orderId);
        }
        if(item == null) {
            item = new MchNotify();
            model.put("item", item);
            return "mch_notify/view";
        }
        JSONObject object = (JSONObject) JSON.toJSON(item);
        if(item.getCreateTime() != null) object.put("createTime", DateUtil.date2Str(item.getCreateTime()));
        if(item.getUpdateTime() != null) object.put("updateTime", DateUtil.date2Str(item.getUpdateTime()));
        if(item.getLastNotifyTime() != null) object.put("lastNotifyTime", DateUtil.date2Str(item.getLastNotifyTime()));
        model.put("item", object);
        return "mch_notify/view";
    }
}