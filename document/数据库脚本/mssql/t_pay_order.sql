
CREATE TABLE [dbo].[t_pay_order](
	[PayOrderId] [varchar](30) NOT NULL,
	[MchId] [varchar](30) NOT NULL,
	[MchOrderNo] [varchar](30) NOT NULL,
	[ChannelId] [varchar](24) NOT NULL,
	[Amount] [bigint] NOT NULL,
	[Currency] [varchar](3) NOT NULL,
	[Status] [int] NOT NULL,
	[ClientIp] [varchar](32) NULL,
	[Device] [varchar](64) NULL,
	[Subject] [varchar](64) NOT NULL,
	[Body] [varchar](256) NOT NULL,
	[Extra] [varchar](512) NULL,
	[ChannelMchId] [varchar](32) NOT NULL,
	[ChannelOrderNo] [varchar](64) NULL,
	[ErrCode] [varchar](64) NULL,
	[ErrMsg] [varchar](128) NULL,
	[Param1] [varchar](256) NULL,
	[Param2] [varchar](256) NULL,
	[NotifyUrl] [varchar](128) NOT NULL,
	[NotifyCount] [int] NOT NULL,
	[LastNotifyTime] [bigint] NULL,
	[ExpireTime] [bigint] NULL,
	[PaySuccTime] [bigint] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK__t_pay_or__E0EF5B9F1367E606] PRIMARY KEY CLUSTERED 
(
	[PayOrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Curre__15502E78]  DEFAULT ('cny') FOR [Currency]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Statu__164452B1]  DEFAULT ((0)) FOR [Status]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Clien__173876EA]  DEFAULT (NULL) FOR [ClientIp]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Devic__182C9B23]  DEFAULT (NULL) FOR [Device]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Extra__1920BF5C]  DEFAULT (NULL) FOR [Extra]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Chann__1A14E395]  DEFAULT (NULL) FOR [ChannelOrderNo]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__ErrCo__1B0907CE]  DEFAULT (NULL) FOR [ErrCode]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__ErrMs__1BFD2C07]  DEFAULT (NULL) FOR [ErrMsg]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Param__1CF15040]  DEFAULT (NULL) FOR [Param1]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Param__1DE57479]  DEFAULT (NULL) FOR [Param2]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Notif__1ED998B2]  DEFAULT ((0)) FOR [NotifyCount]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__LastN__1FCDBCEB]  DEFAULT (NULL) FOR [LastNotifyTime]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Expir__20C1E124]  DEFAULT (NULL) FOR [ExpireTime]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__PaySu__21B6055D]  DEFAULT (NULL) FOR [PaySuccTime]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Creat__22AA2996]  DEFAULT (getdate()) FOR [CreateTime]
GO

ALTER TABLE [dbo].[t_pay_order] ADD  CONSTRAINT [DF__t_pay_ord__Updat__239E4DCF]  DEFAULT (getdate()) FOR [UpdateTime]
GO


