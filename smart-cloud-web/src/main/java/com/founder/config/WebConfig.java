package com.founder.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="config.web")
public class WebConfig {

    private String base_url;
    private String req_key;
    private String rep_key;
    private String currency;
    private String device;
    private String notify_url;
    private Boolean execute_notify;
    private Boolean execute_query;
    private Boolean execute_sync;//是否两个库同步订单

    public String getBase_url() {
        return base_url;
    }

    public void setBase_url(String base_url) {
        this.base_url = base_url;
    }

    public String getReq_key() {
        return req_key;
    }

    public void setReq_key(String req_key) {
        this.req_key = req_key;
    }

    public String getRep_key() {
        return rep_key;
    }

    public void setRep_key(String rep_key) {
        this.rep_key = rep_key;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public Boolean getExecute_notify() {
        return execute_notify;
    }

    public void setExecute_notify(Boolean execute_notify) {
        this.execute_notify = execute_notify;
    }

    public Boolean getExecute_query() {
        return execute_query;
    }

    public void setExecute_query(Boolean execute_query) {
        this.execute_query = execute_query;
    }

    public Boolean getExecute_sync() {
        return execute_sync;
    }

    public void setExecute_sync(Boolean execute_sync) {
        this.execute_sync = execute_sync;
    }
}
