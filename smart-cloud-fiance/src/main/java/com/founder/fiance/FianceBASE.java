package com.founder.fiance;

import com.founder.core.domain.FianceAl;
import com.founder.core.domain.FianceWx;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public abstract class FianceBASE {

    public abstract String buildContent(List<?> list, String subsys);

    public List<FianceWx> toFianceWxList(List<?> list){
        List<FianceWx> fianceWxList = new ArrayList<>();
        list.forEach(item ->{
            fianceWxList.add((FianceWx) item);
        });
        return fianceWxList;
    }

    public List<FianceAl> toFianceAlList(List<?> list){
        List<FianceAl> fianceAlList = new ArrayList<>();
        list.forEach(item ->{
            fianceAlList.add((FianceAl) item);
        });
        return fianceAlList;
    }
}
